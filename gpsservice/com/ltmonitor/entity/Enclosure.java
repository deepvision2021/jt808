﻿package com.ltmonitor.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;



@Entity
@Table(name="Enclosure")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
public class Enclosure extends TenantEntity implements Serializable
{
	public static String POLYGON = "polygon";
	public static String RECT = "rect";
	public static String CIRCLE = "circle";
	public static String ROUTE = "route";

	public static final String InDriver = "进区域报警给驾驶员";
	public static final String InPlatform = "进区域报警给平台";
	public static final String OutDriver = "出区域报警给驾驶员";
	public static final String OutPlatform = "出区域报警给平台";

	public Enclosure()
	{
		setCreateDate(new java.util.Date());
		setStartDate(new java.util.Date());
		setEndDate(new java.util.Date());
		this.maxSpeed = 89;
		this.delay = 10;
		this.limitSpeed = true;
		this.setLineWidth(50);
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "enclosureId", unique = true, nullable = false)
	private int entityId;
	public  int getEntityId() {
		return entityId;
	}
	public  void setEntityId(int value) {
		entityId = value;
	}
	

	private String plateNo;
	public final String getPlateNo()
	{
		return plateNo;
	}
	public final void setPlateNo(String value)
	{
		plateNo = value;
	}

	//车基ID
	private String gpsId;
	public final String getGpsId()
	{
		return gpsId;
	}
	public final void setGpsId(String value)
	{
		gpsId = value;
	}
	//围栏名称
	private String name;
	public final String getName()
	{
		return name;
	}
	public final void setName(String value)
	{
		name = value;
	}
	//围栏坐标点,字符串格式是x1,y1;x2,y2,分别代表左上角和右下角的坐标点值
	private String points;
	public final String getPoints()
	{
		return points;
	}
	public final void setPoints(String value)
	{
		points = value;
	}
	//围栏号
	private int sn;
	public final int getSn()
	{
		return sn;
	}
	public final void setSn(int value)
	{
		sn = value;
	}
	//围栏类型
	private String enclosureType;
	public final String getEnclosureType()
	{
		return enclosureType;
	}
	public final void setEnclosureType(String value)
	{
		enclosureType = value;
	}
	/**
	 * 关键点监控
	 */
	private int keyPoint;
	//报警类型，每个报警使用逗号隔开
	private String alarmType;
	public final String getAlarmType()
	{
		return alarmType;
	}
	public final void setAlarmType(String value)
	{
		alarmType = value;
	}
	//围栏有效期
	private java.util.Date startDate = new java.util.Date(0);
	public final java.util.Date getStartDate()
	{
		return startDate;
	}
	public final void setStartDate(java.util.Date value)
	{
		startDate = value;
	}

	private java.util.Date endDate = new java.util.Date(0);
	public final java.util.Date getEndDate()
	{
		return endDate;
	}
	public final void setEndDate(java.util.Date value)
	{
		endDate = value;
	}

	//半径，以地图的米为单位
	private double radius;
	public final double getRadius()
	{
		return radius;
	}
	public final void setRadius(double value)
	{
		radius = value;
	}
	//是否根据时间
	private boolean byTime;
	public final boolean getByTime()
	{
		return byTime;
	}
	public final void setByTime(boolean value)
	{
		byTime = value;
	}
	//是否限速
	private boolean limitSpeed;
	public final boolean getLimitSpeed()
	{
		return limitSpeed;
	}
	public final void setLimitSpeed(boolean value)
	{
		limitSpeed = value;
	}
	/**
	 * 偏移报警延迟时间
	 * 
	 */
	private int offsetDelay;

	//超速持续时间
	private int delay;
	public final int getDelay()
	{
		return delay;
	}
	public final void setDelay(int value)
	{
		delay = value;
	}
	//最大速度限制
	private double maxSpeed;
	public final double getMaxSpeed()
	{
		return maxSpeed;
	}
	public final void setMaxSpeed(double value)
	{
		maxSpeed = value;
	}

	private String status;
	public final String getStatus()
	{
		return status;
	}
	public final void setStatus(String value)
	{
		status = value;
	}
	//统一线宽
	private double lineWidth;
	public final double getLineWidth()
	{
		return lineWidth;
	}
	public final void setLineWidth(double value)
	{
		lineWidth = value;
	}
	//围栏的中心
	private double centerLat;
	
	private double centerLng;
	

	//public double Longitude { get; set; }

	//public double Latitude { get; set; }

	//创建区域属性
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: public ushort CreateAreaAttr()
	public final short CreateAreaAttr()
	{
		byte[] bytes = new byte[16];
		bytes[0] = (byte)(getByTime() ? 1 : 0); //1：根据时间
		bytes[1] = (byte)(getLimitSpeed() ? 1 : 0); //限速
		bytes[2] = (byte)(getAlarmType().indexOf(InDriver) >= 0 ? 1 : 0);
		bytes[3] = (byte)(getAlarmType().indexOf(InPlatform) >= 0 ? 1 : 0);
		bytes[4] = (byte)(getAlarmType().indexOf(OutDriver) >= 0 ? 1 : 0);
		bytes[5] = (byte)(getAlarmType().indexOf(OutPlatform) >= 0 ? 1 : 0);
		bytes[6] = 0;
		bytes[7] = 0;
		bytes[8] = 0; //0: 关闭 1：启动限速拍照

		bytes[15] = 0; //15 0：无区域名称 1：有区域名称

		String byteStr = ""; //二进制字符创
		for (int m = 0; m < 16; m++)
		{
			byteStr += bytes[15 - m];
			//byteStr += bytes[m];
		}

//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: ushort t = Convert.ToUInt16(byteStr, 2);
		//short t = Short.parseShort(byteStr, 2); //将二进制字符转换成ushort
		short t = (short)Integer.parseInt(byteStr, 2);
		return t;
	}
	//区域的每个顶点
	public final java.util.ArrayList<EnclosureNode> GetNodes()
	{
		java.util.ArrayList<EnclosureNode> nodes = new java.util.ArrayList<EnclosureNode>();

		String[] strPts = getPoints().split("[;]", -1);
		for (String strPt : strPts)
		{
			if (StringUtil.isNullOrEmpty(strPt) == false)
			{
				String[] strPoint = strPt.split("[,]", -1);
				if (strPoint.length == 2)
				{
					EnclosureNode pl = new EnclosureNode();
					pl.setLng(Double.parseDouble(strPoint[0]));
					pl.setLat(Double.parseDouble(strPoint[1]));
					nodes.add(pl);
				}
			}
		}
		if(this.enclosureType.equals(Enclosure.POLYGON))
		{
			nodes.remove(nodes.size() - 1);
		}
		return nodes;
	}
	public double getCenterLat() {
		return centerLat;
	}
	public void setCenterLat(double centerLat) {
		this.centerLat = centerLat;
	}
	public double getCenterLng() {
		return centerLng;
	}
	public void setCenterLng(double centerLng) {
		this.centerLng = centerLng;
	}
	public int getOffsetDelay() {
		return offsetDelay;
	}
	public void setOffsetDelay(int offsetDelay) {
		this.offsetDelay = offsetDelay;
	}
	public int getKeyPoint() {
		return keyPoint;
	}
	public void setKeyPoint(int keyPoint) {
		this.keyPoint = keyPoint;
	}
	

}