package com.ltmonitor.jt808.service;

import java.util.Date;

public class AlarmItem {
	private Date alarmTime;

	private String alarmKey;

	private boolean open;

	public AlarmItem(String _key, Date _alarmTime) {
		alarmTime = _alarmTime;
		this.alarmKey = _key;
		open = true;

	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public String getAlarmKey() {
		return alarmKey;
	}

	public void setAlarmKey(String alarmKey) {
		this.alarmKey = alarmKey;
	}

	public Date getAlarmTime() {
		return alarmTime;
	}

	public void setAlarmTime(Date alarmTime) {
		this.alarmTime = alarmTime;
	}
}