package com.ltmonitor.jt808.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import com.ltmonitor.dao.IBaseDao;
import com.ltmonitor.entity.AlarmRecord;
import com.ltmonitor.entity.Enclosure;
import com.ltmonitor.entity.GPSRealData;
import com.ltmonitor.entity.StringUtil;
import com.ltmonitor.entity.VehicleData;
import com.ltmonitor.jt808.service.AlarmItem;
import com.ltmonitor.jt808.service.IAlarmService;
import com.ltmonitor.jt808.service.ITransferGpsService;
import com.ltmonitor.util.DateUtil;

/**
 * 报警分析服务
 * 
 * @author DELL
 * 
 */
public class AlarmService implements IAlarmService {
	public static String TURN_ON = "1"; // 报警开
	public static String TURN_OFF = "0"; // 报警关闭

	private static Logger logger = Logger.getLogger(AlarmService.class);
	private ConcurrentLinkedQueue<GPSRealData> dataQueue = new ConcurrentLinkedQueue();

	public ConcurrentMap<String, GPSRealData> oldRealDataMap = new ConcurrentHashMap<String, GPSRealData>();
	private IBaseDao baseDao;

	private Thread processRealDataThread;


	//内存中的保留的已经发生的报警
	private ConcurrentHashMap<String, AlarmItem> alarmMap = new ConcurrentHashMap<String, AlarmItem>();
	private Boolean startAnalyze = true;
	private ITransferGpsService transferGpsService;
	
	
	private boolean parkingAlarmEnabled;

	public AlarmService() {
		processRealDataThread = new Thread(new Runnable() {
			public void run() {
				processRealDataThreadFunc();
			}
		});

		processRealDataThread.start();

	}

	@Override
	public void stopService() {
		startAnalyze = false;
		try {
			processRealDataThread.join(50000);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		// processRealDataThread.stop();
	}

	
	@Override
	public void processRealData(GPSRealData rd) {
		//if (processRealDataThread.isAlive() == false) {
		//}
		dataQueue.add(rd);
	}


	private void processRealDataThreadFunc() {
		ExecutorService fixedThreadPool = Executors.newFixedThreadPool(3);
		while (startAnalyze) {
			try {
				GPSRealData tm = dataQueue.poll();
				final List<GPSRealData> msgList = new ArrayList<GPSRealData>();
				while (tm != null) {
					msgList.add(tm);
					if (msgList.size() > 30)
						break;
					tm = dataQueue.poll();
				}
				if (msgList.size() > 0) {
					fixedThreadPool.execute(new Runnable() {
						@Override
						public void run() {
							analyzeData(msgList);
							try {
								Thread.sleep(2000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					});
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}

			try {
				Thread.sleep(1000L);
			} catch (InterruptedException e1) {
			}
		}
	}

	private void analyzeData(List<GPSRealData> msgList) {

		for (GPSRealData msg : msgList) {
			try {
				analyzeData(msg);
			} catch (Exception ex) {
				logger.error(ex.getMessage(), ex);
			}
		}

	}

	private void analyzeData(GPSRealData rd) {
		try {
			System.out.println(rd.getSimNo());
			GPSRealData oldRd = GetOldRealData(rd.getSimNo());
			String oldStatus = oldRd.getStatus();
			String newStatus = rd.getStatus();
			String oldAlarmState = oldRd.getAlarmState();
			String newAlarmState = rd.getAlarmState();
			createChangeRecord(AlarmRecord.STATE_FROM_TERM, oldStatus,
					newStatus, rd);
			createChangeRecord(AlarmRecord.ALARM_FROM_TERM, oldAlarmState,
					newAlarmState, rd);

			//停车报警
			if(parkingAlarmEnabled)
				parkingAlarm(rd);
			
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		// 保存在缓存中，等待下一次数据比对
		/**
		 * GPSRealData oldRd = oldRealDataMap.get(rd.getSimNo()); if (oldRd ==
		 * null || oldRd.getOnline() == false) { // 上线记录
		 * this.createOnlineChangeRecord(rd); }
		 */
		oldRealDataMap.put(rd.getSimNo(), rd);
	}
	
	private void parkingAlarm(GPSRealData rd)
	{
		String alarmKey = rd.getPlateNo() + "_"+AlarmRecord.TYPE_PARKING;
		//停车报警
		if(rd.getVelocity() < 1)
		{
			if (alarmMap.containsKey(alarmKey)) {
				AlarmItem item = alarmMap.get(alarmKey);
			}else
			{
				//第一次停车
				AlarmItem item = new AlarmItem(alarmKey, rd.getSendTime());
				this.CreateWarnRecord(AlarmRecord.ALARM_FROM_TERM, AlarmRecord.TYPE_PARKING, TURN_ON, rd);
				alarmMap.put(alarmKey, item);//报警标志驻留在内存中，留待下次判断是否已经报警
			}
		}else
		{
			//停车报警结束
			if (alarmMap.containsKey(alarmKey)) {
				AlarmItem item = alarmMap.get(alarmKey); //如果有产生的报警，则需要消除报警
				item.setOpen(false);
				alarmMap.remove(alarmKey);
				this.CreateWarnRecord(AlarmRecord.ALARM_FROM_TERM, AlarmRecord.TYPE_PARKING, TURN_OFF, rd);
			}
		}
	}

	/**
	 * 创建状态位变化的记录，记录变化的起止时间和间隔，及经纬度坐标
	 * 
	 * @param type
	 * @param oldStatus
	 * @param newStatus
	 * @param rd
	 */
	private void createChangeRecord(String type, String oldStatus,
			String newStatus, GPSRealData rd) {
		if (StringUtil.isNullOrEmpty(oldStatus)
				|| StringUtil.isNullOrEmpty(newStatus)
				|| oldStatus.length() != newStatus.length()
				|| oldStatus.equals(newStatus))
			return;

		char[] oldChars = (char[]) oldStatus.toCharArray();

		char[] newChars = (char[]) newStatus.toCharArray();

		for (int m = 0; m < oldChars.length; m++) {
			if (oldChars[m] != newChars[m]) {
				int alarmId = 31 - m; // 倒序，转换为部标的报警序号
				String alarmType = "" + alarmId;
				String alarmState = "" + newChars[m];

				if (alarmId == 20) {
					alarmType = rd.getEnclosureAlarm() == 0 ? AlarmRecord.TYPE_IN_AREA
							: AlarmRecord.TYPE_CROSS_BORDER;
					// alarmState = ""+rd.getEnclosureAlarm();
				} else if (alarmId == 21) {
					alarmType = rd.getEnclosureAlarm() == 0 ? AlarmRecord.TYPE_ON_ROUTE
							: AlarmRecord.TYPE_OFFSET_ROUTE;
					// alarmState = ""+rd.getEnclosureAlarm();
				}

				CreateWarnRecord(type, alarmType, alarmState, rd);
			}
		}
	}

	/**
	 * 创建报警记录 OperateType表示类型，状态位变化(State)还是报警标志位变化(Warn)， childType
	 * 表示标志位的字节32位序号, 如Acc标志位在32个状态位的第一个位置,超速报警在报警位的第二个位置. warnState “1”代表报警 ，
	 * 0代表报警消除
	 */
	private void CreateWarnRecord(String OperateType, String childType,
			String warnState, GPSRealData rd) {
		AlarmRecord sr = CreateRecord(OperateType, childType, warnState, rd);
		if (sr != null)
			getBaseDao().saveOrUpdate(sr);
	}

	/**
	 * 将报警转发给上级平台
	 */
	private void transferAlarm(AlarmRecord ar, GPSRealData rd) {
		this.transferGpsService.transfer(ar, rd);
	}

	private AlarmRecord CreateRecord(String OperateType, String childType,
			String alarmState, GPSRealData rd) {
		String hsql = "from AlarmRecord rec where startTime > ? and  rec.plateNo = ? and rec.status = ? and rec.type = ? and rec.childType = ?";
		// 查看是否有未消除的报警记录
		Date startDate = DateUtil.getDate(new Date(), Calendar.DAY_OF_YEAR, -5);
		AlarmRecord sr = (AlarmRecord) getBaseDao().find(
				hsql,
				new Object[] { startDate, rd.getPlateNo(),
						AlarmRecord.STATUS_NEW, OperateType, childType });

		if (sr == null) {
			if (TURN_OFF.equals(alarmState))
				return null;

			sr = new AlarmRecord();
			// 停车报警
			if (childType.equals("19")) {
				/**
				 * Enclosure ec = IsInEnclosure(rd); if (ec != null) {
				 * sr.Station = ec.Name; sr.Location = ec.Name; }
				 */
			}

			if (rd.getEnclosureId() > 0) {
				String hql = "from Enclosure where enclosureId = ?";
				Enclosure ec = (Enclosure) baseDao.find(hql,
						rd.getEnclosureId());
				if (ec != null) {
					sr.setLocation(ec.getName());
				}

			}
			sr.setType(OperateType);
			sr.setPlateNo(rd.getPlateNo());
			sr.setStartTime(rd.getSendTime());
			sr.setStatus(AlarmRecord.STATUS_NEW);
			sr.setEndTime(new Date());
			sr.setLatitude(rd.getLatitude());
			sr.setLongitude(rd.getLongitude());
			sr.setVelocity(rd.getVelocity());
			sr.setChildType(childType);

			if (AlarmRecord.ALARM_FROM_TERM.equals(sr.getType()))
				this.transferAlarm(sr, rd);// 转发给809政府平台
		} else {
			sr.setEndTime(new Date());
			double minutes = DateUtil.getSeconds(sr.getStartTime(),
					rd.getSendTime()) / 60;
			sr.setTimeSpan(minutes);// 计算出报警时长
			if (alarmState.equals(TURN_OFF)) {
				sr.setStatus(AlarmRecord.STATUS_OLD);
				sr.setEndTime(rd.getSendTime());

				sr.setLatitude1(rd.getLatitude());
				sr.setLongitude1(rd.getLongitude());
			} else
				return null;

		}

		sr.setType(OperateType);
		sr.setChildType(childType);
		return sr;
	}

	private AlarmRecord CreateEnclosureRecord(String OperateType,
			String childType, String warnState, GPSRealData rd) {
		String hsql = "from AlarmRecord rec where startTime > ? and  rec.plateNo = ? and rec.status = ? and rec.type = ? and rec.childType = ?";
		// 查看是否有未消除的报警记录
		Date startDate = DateUtil.getDate(new Date(), Calendar.DAY_OF_YEAR, -5);
		AlarmRecord sr = (AlarmRecord) getBaseDao().find(
				hsql,
				new Object[] { startDate, rd.getPlateNo(),
						AlarmRecord.STATUS_NEW, OperateType, childType });

		if (sr == null) {
			if (TURN_OFF.equals(warnState))
				return null;

			sr = new AlarmRecord();
			// 停车报警
			if (rd.getEnclosureId() > 0) {
				String hql = "from Enclosure where enclosureId = ?";
				Enclosure ec = (Enclosure) baseDao.find(hsql,
						rd.getEnclosureId());
			}

			sr.setPlateNo(rd.getPlateNo());
			sr.setStartTime(rd.getSendTime());
			sr.setStatus(AlarmRecord.STATUS_NEW);
			sr.setEndTime(new Date());
			sr.setLatitude(rd.getLatitude());
			sr.setLongitude(rd.getLongitude());
			// sr.setLocation( MapFix.GetLocation(sr.Longitude, sr.Latitude));
			sr.setVelocity(rd.getVelocity());
		} else {
			sr.setEndTime(new Date());
			double minutes = DateUtil.getSeconds(sr.getStartTime(),
					rd.getSendTime()) / 60;
			sr.setTimeSpan(minutes);// 计算出报警时长
			if (warnState.equals(TURN_OFF)) {
				sr.setStatus(AlarmRecord.STATUS_OLD);
				sr.setEndTime(rd.getSendTime());

				sr.setLatitude1(rd.getLatitude());
				sr.setLongitude1(rd.getLongitude());
			} else
				return null;

		}

		sr.setType(OperateType);
		sr.setChildType(childType);
		return sr;
	}


	private GPSRealData getRalData(String plateNo) {
		GPSRealData rd = oldRealDataMap.get(plateNo);
		if (rd == null) {
			rd = new GPSRealData();
			rd.setOnline(false);
		}
		return rd;
	}


	// 创建上线下线状态变化的记录，记录变化的起止时间和间隔，及经纬度坐标
	@Override
	public void createOnlineChangeRecord(GPSRealData rd, String OperateType) {
		Boolean IsOnline = rd.getOnline();
		Boolean onlineTag = OperateType == AlarmRecord.TYPE_ONLINE;
		String hsql = "from AlarmRecord rec where rec.plateNo = ? and rec.status = ? and rec.childType = ?";
		// 查看是否有未消除的报警记录
		AlarmRecord sr = (AlarmRecord) getBaseDao().find(
				hsql,
				new String[] { rd.getPlateNo(), AlarmRecord.STATUS_NEW,
						OperateType });

		if (sr == null && onlineTag == IsOnline) {
			sr = new AlarmRecord();
			sr.setPlateNo(rd.getPlateNo());
			sr.setStartTime(rd.getSendTime());
			sr.setStatus(AlarmRecord.STATUS_NEW);
			sr.setEndTime(new Date());
			sr.setLatitude(rd.getLatitude());
			sr.setLongitude(rd.getLongitude());
			sr.setType(AlarmRecord.ALARM_FROM_PLATFORM);// 平台报警

			sr.setVelocity(rd.getVelocity());
			sr.setChildType(OperateType);
			getBaseDao().saveOrUpdate(sr);
		} else if (onlineTag != IsOnline && sr != null) {
			sr.setEndTime(rd.getSendTime());
			double t = DateUtil.getSeconds(sr.getStartTime(), rd.getSendTime()) / 60;
			sr.setTimeSpan(t);

			sr.setStatus(AlarmRecord.STATUS_OLD);
			sr.setEndTime(new Date());// rd.SendTime;
			sr.setLatitude1(rd.getLatitude());
			sr.setLongitude1(rd.getLongitude());

			sr.setChildType(OperateType);
			getBaseDao().saveOrUpdate(sr);
		}
	}

	/**
	 * 从缓冲中取出旧的实时数据，进行比对，分析报警状态位和上线状态的变化
	 */
	@Override
	public GPSRealData GetOldRealData(String simNo) {
		GPSRealData oldRd = null;
		if (oldRealDataMap.containsKey(simNo)) {
			oldRd = (GPSRealData) oldRealDataMap.get(simNo);
		}

		if (oldRd == null) {
			String hsql = "from VehicleData where simNo = ?";
			VehicleData vd = (VehicleData) baseDao.find(hsql, simNo);
			if (vd != null) {
				hsql = "from GPSRealData where vehicleId = ?";
				oldRd = (GPSRealData) baseDao.find(hsql, vd.getEntityId());
				if (oldRd == null)
					oldRd = new GPSRealData();
				oldRd.setVehicleId(vd.getEntityId());
				oldRd.setPlateNo(vd.getPlateNo());
			} else {
				return null;// new GPSRealData();
			}

			oldRd.setOnline(false);
			oldRd.setAlarmState(String.format("%032d", 0));
			oldRd.setStatus(String.format("%032d", 0));
			oldRd.setSendTime(new Date());
			oldRd.setSimNo(simNo);
			oldRealDataMap.put(simNo, oldRd);
		}

		return oldRd;
	}

	public IBaseDao getBaseDao() {
		return baseDao;
	}

	public void setBaseDao(IBaseDao baseDao) {
		this.baseDao = baseDao;
	}

	public ITransferGpsService getTransferGpsService() {
		return transferGpsService;
	}

	public void setTransferGpsService(ITransferGpsService transferGpsService) {
		this.transferGpsService = transferGpsService;
	}


	public boolean isParkingAlarmEnabled() {
		return parkingAlarmEnabled;
	}

	public void setParkingAlarmEnabled(boolean parkingAlarmEnabled) {
		this.parkingAlarmEnabled = parkingAlarmEnabled;
	}

}
