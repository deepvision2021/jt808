﻿package com.ltmonitor.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * 基于saas的多租户模型
 * @author DELL
 *
 */
@MappedSuperclass
public abstract class TenantEntity implements IEntity, Serializable {
	private int tenantId;

	public  int getTenantId() {
		return tenantId;
	}

	public  void setTenantId(int value) {
		tenantId = value;
	}
	/**
	private int sortId;

	public  int getSortId() {
		return sortId;
	}

	public  void setSortId(int value) {
		sortId = value;
	}
	*/

	// 创建日期
	@Column(name = "createDate")
	protected java.util.Date createDate = new java.util.Date(0);

	public  java.util.Date getCreateDate() {
		return createDate;
	}

	public  void setCreateDate(java.util.Date value) {
		createDate = value;
	}

	// 备注
	@Column(name = "remark")
	private String remark;

	public  String getRemark() {
		return remark;
	}

	public  void setRemark(String value) {
		remark = value;
	}

	// 假删除标记
	@Column(name = "deleted")
	private boolean deleted;

	public  boolean getDeleted() {
		return deleted;
	}

	public  void setDeleted(boolean value) {
		deleted = value;
	}

	// 数据的创建者
	@Column(name = "owner")
	private String owner;

	public  String getOwner() {
		return owner;
	}

	public  void setOwner(String value) {
		owner = value;
	}

}